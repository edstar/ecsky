/* Copyright (C) 2018, 2019, 2020, 2021 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2018, 2019 CNRS, Université Paul Sabatier
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef ECSKY_C_H
#define ECSKY_C_H

#include "ecsky.h"

#include <high_tune/ecrp.h>

#include <rsys/logger.h>
#include <rsys/mem_allocator.h>
#include <rsys/ref_count.h>
#include <rsys/str.h>

/* Declare some constants */
#define DRY_AIR_MOLAR_MASS 0.0289644 /* In kg.mol^-1 */
#define H2O_MOLAR_MASS 0.01801528 /* In kg.mol^-1 */
#define GAS_CONSTANT 8.3144598 /* In kg.m^2.s^-2.mol^-1.K */

/* Forward declaration of external data types */
struct ecrp;
struct htgop;
struct svx_tree;
struct svx_tree_desc;

/* Forward declarations of internal data types */
struct atmosphere;

struct split {
  size_t index; /* Index of the current ecrp voxel */
  double height; /* Absolute height where the next voxel starts */
};

#define DARRAY_NAME split
#define DARRAY_DATA struct split
#include <rsys/dynamic_array.h>

struct ecsky {
  struct cloud** clouds; /* Per sw_band cloud data structure */

  /* Per sw_band and per quadrature point atmosphere data structure */
  struct atmosphere** atmosphere;

  /* Loaders of... */
  struct ecrp* ecrp;   /* ... Cloud properties */
  struct htgop* htgop; /* ... Gas optical properties */

  /* Star-VX library handle */
  struct svx_device* svx;

  struct ecrp_desc ecrp_desc; /* Descriptor of the loaded LES data */

  size_t ncomponents; /* number of components in radiative properties */

  /* LUT used to map the index of a Z from the regular SVX to the irregular
   * ECRP data */
  struct darray_split svx2ecrp_z;
  double lut_cell_sz; /* Size of a svx2ecrp_z cell */

  /* Ids and optical properties of spectral bands loaded by HTGOP and currently
   * handled by the sky */
  enum ecsky_spectral_type spectral_type;
  size_t bands_range[2]; /* Inclusive band ids */

  int repeat_clouds; /* Make clouds infinite in X and Y */
  int is_cloudy; /* The sky has clouds */

  unsigned nthreads; /* #threads */

  struct str name; /* Name of the sky */

  struct mem_allocator* allocator;
  struct mem_allocator svx_allocator;
  struct logger* logger;
  struct logger logger__; /* Default logger */
  int verbose;
  ref_T ref;
};

static FINLINE double
wavenumber_to_wavelength(const double nu/*In cm^-1*/)
{
  return 1.e7 / nu;
}

/* In cm^-1 */
static FINLINE double
wavelength_to_wavenumber(const double lambda/*In nanometer*/)
{
  return wavenumber_to_wavelength(lambda);
}

/* Transform a position from world to cloud space */
extern LOCAL_SYM double*
world_to_cloud
  (const struct ecsky* sky,
   const double pos_ws[3], /* World space position */
   double out_pos_cs[3]);

#endif /* ECSKY_C_H */
